# TP1 - Linux,  (re)Familiaration avec un système GNU/Linux
## 0. Préparation de la machine

***l'accès a internet :***

*pour voir la carte réseau dédiée (VM 1 et 2):*
commande : `ip a`
VM 1 : 
```
enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:2a:e3:95 brd ff:ff:ff:ff:ff:ff
    inet 10.101.1.11/24 brd 10.101.1.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe2a:e395/64 scope link
       valid_lft forever preferred_lft forever
```

VM 2 :
```
enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:d1:a2:11 brd ff:ff:ff:ff:ff:ff
    inet 10.101.1.12/24 brd 10.101.1.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fed1:a211/64 scope link
       valid_lft forever preferred_lft forever
```

*pour voir la route par défault (VM 1 et 2):*
commande : `ip route show` ou `ip r s`

VM 1 :
```
default via 192.168.1.254 dev enp0s8 proto static metric 100
```

VM 2 :
```
default via 192.168.1.254 dev enp0s8 proto static metric 101
```

les machines peuvent se ping avec la commande : `ping`

VM 1 :
```
[leo@node1 ~]$ ping 10.101.1.12
PING 10.101.1.12 (10.101.1.12) 56(84) bytes of data.
64 bytes from 10.101.1.12: icmp_seq=1 ttl=64 time=0.239 ms
64 bytes from 10.101.1.12: icmp_seq=2 ttl=64 time=0.349 ms
64 bytes from 10.101.1.12: icmp_seq=3 ttl=64 time=0.312 ms
64 bytes from 10.101.1.12: icmp_seq=4 ttl=64 time=0.321 ms
--- 10.101.1.12 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3056ms
rtt min/avg/max/mdev = 0.239/0.305/0.349/0.042 ms
```

VM 2 : 
```
[leo@node2 ~]$ ping 10.101.1.12
PING 10.101.1.12 (10.101.1.12) 56(84) bytes of data.
64 bytes from 10.101.1.12: icmp_seq=1 ttl=64 time=0.025 ms
64 bytes from 10.101.1.12: icmp_seq=2 ttl=64 time=0.027 ms
64 bytes from 10.101.1.12: icmp_seq=3 ttl=64 time=0.028 ms
64 bytes from 10.101.1.12: icmp_seq=4 ttl=64 time=0.029 ms
--- 10.101.1.12 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3063ms
rtt min/avg/max/mdev = 0.025/0.027/0.029/0.004 ms
```

les machines sont en IP statiques : 
commande : `sudo nano /etc/sysconfig/network-scripts/ifcfg-enp0s8`

resultat :
VM 1 : 
```
NAME=enp0s8
DEVICE=enp0s8

BOOTPROTO=static
ONBOOT=yes

IPADDR=10.101.1.11
NETMASK=255.255.255.0

GATEWAY=192.168.1.254
DNS=1.1.1.1
```

VM 2 : 
```
NAME=enp0s8
DEVICE=enp0s8

BOOTPROTO=static
ONBOOT=yes

IPADDR=10.101.1.12
NETMASK=255.255.255.0

GATEWAY=192.168.1.254
DNS=1.1.1.1
```

les machines sont nommées `node1.tp1.b2` et `node2.tp1.b2` grace a la commande : `sudo hostname`

modification du fichier "hosts" dans `/etc/hosts`

VM 1 :
```
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
10.101.1.11 node1.tp1.b2
10.101.1.12 node2.tp1.b2
```

VM 2 : 
```
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
10.101.1.12 node2.tp1.b2
10.101.1.11 node1.tp1.b2
```

ping des VM avec leurs nom :

commande : `ping <NOM>`

VM 1 : 
```
[leo@node1 ~]$ ping node2.tp1.b2
PING node2.tp1.b2 (10.101.1.12) 56(84) bytes of data.
64 bytes from node2.tp1.b2 (10.101.1.12): icmp_seq=1 ttl=64 time=0.277 ms
64 bytes from node2.tp1.b2 (10.101.1.12): icmp_seq=2 ttl=64 time=0.371 ms
64 bytes from node2.tp1.b2 (10.101.1.12): icmp_seq=3 ttl=64 time=0.345 ms
64 bytes from node2.tp1.b2 (10.101.1.12): icmp_seq=4 ttl=64 time=0.482 ms
--- node2.tp1.b2 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3076ms
rtt min/avg/max/mdev = 0.277/0.368/0.482/0.077 ms
```

VM 2 : 
```
[leo@node2 ~]$ ping node1.tp1.b2
PING node1.tp1.b2 (10.101.1.11) 56(84) bytes of data.
64 bytes from node1.tp1.b2 (10.101.1.11): icmp_seq=1 ttl=64 time=0.217 ms
64 bytes from node1.tp1.b2 (10.101.1.11): icmp_seq=2 ttl=64 time=0.360 ms
64 bytes from node1.tp1.b2 (10.101.1.11): icmp_seq=3 ttl=64 time=0.504 ms
64 bytes from node1.tp1.b2 (10.101.1.11): icmp_seq=4 ttl=64 time=0.352 ms
--- node1.tp1.b2 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3097ms
rtt min/avg/max/mdev = 0.217/0.358/0.504/0.102 ms
```

## I. Utilisateurs
### 1. Création et configuration

création d'un user avec un repertoire spécifique pour node1 et node2
avec la commande : `sudo useradd leoS --home /home/leoS -s /bin/bash`

je créer un groupe admins avec la commande : `groupadd admins`

j'ajoute le groupe au sudoers, en modifiant le fichier avec visudo : 
`sudo visuado /etc/sudoers`

et je rajoute la ligne : `%admins ALL=(ALL) ALL`

j'ajoute l'user a mon groupe admin avec la commande : `sudo usermod -aG admins leoS`


### 2. SSH

je génère une clé ssh sur ma machine hote avec la commande : `ssh-keygen -t rsa -b 4096`

sur ma machine hote j'ouvre gitBash pour envoyer ma clé publique sur node1
avec la commande : `$ ssh-copy-id -i ~/.ssh/id_rsa.pub leo@10.101.1.11`

et ensuite je me connecte en ssh à ma VM avec la commande `ssh leo@10.101.1.11`

## II. Partitionnement

dans virtualBox j'ajoute 2 disques physiques a ma VM.

dans node1 j'utilise la commande : `sudo pvcreate /dev/sdb` et `sudo pvcreate /dev/sdb` pour créer des volumes physiques.

creation des volumes groupes avec la commande : `sudo vgcreate secdsk /dev/sdb` et `sudo vgextend secdsk /dev/sdc`

création de 3 volumes logiques avec la commande : `sudo lvcreate` 
```
[leo@node1 ~]$ sudo lvcreate -L 1024M secdsk -n secdskpart1
  Logical volume "secdskpart1" created.
[leo@node1 ~]$ sudo lvcreate -L 1024M secdsk -n secdskpart2
  Logical volume "secdskpart2" created.
[leo@node1 ~]$ sudo lvcreate -L 1024M secdsk -n secdskpart3
  Logical volume "secdskpart3" created.

```


puis je formate les parties logiques : 

commande : `sudo mkfs -t ext4 <PARTITION>`

resultat : 
```
[leo@node1 ~]$ sudo mkfs -t ext4 /dev/secdsk/secdskpart1
mke2fs 1.45.6 (20-Mar-2020)
Creating filesystem with 262144 4k blocks and 65536 inodes
Filesystem UUID: dd6fc274-fa0e-4f64-a9ee-46a8bc360081
Superblock backups stored on blocks:
        32768, 98304, 163840, 229376

Allocating group tables: done
Writing inode tables: done
Creating journal (8192 blocks): done
Writing superblocks and filesystem accounting information: done

[leo@node1 ~]$ sudo mkfs -t ext4 /dev/secdsk/secdskpart2
mke2fs 1.45.6 (20-Mar-2020)
Creating filesystem with 262144 4k blocks and 65536 inodes
Filesystem UUID: 923370f6-b5a1-4c33-bd6e-1880bc07b7f4
Superblock backups stored on blocks:
        32768, 98304, 163840, 229376

Allocating group tables: done
Writing inode tables: done
Creating journal (8192 blocks): done
Writing superblocks and filesystem accounting information: done

[leo@node1 ~]$ sudo mkfs -t ext4 /dev/secdsk/secdskpart3
mke2fs 1.45.6 (20-Mar-2020)
Creating filesystem with 262144 4k blocks and 65536 inodes
Filesystem UUID: ec2f277d-5258-4f49-bad8-700363f4b8a1
Superblock backups stored on blocks:
        32768, 98304, 163840, 229376

Allocating group tables: done
Writing inode tables: done
Creating journal (8192 blocks): done
Writing superblocks and filesystem accounting information: done

```

et pour finir je monte les partitions : 

je créer les fichiers pour les partitions avec la commande `sudo mkdir -p /mnt/secdskpart1`, `secdskpart2` et `secdskpart3` 

et avec la commande : `mount` je monte les partitions dans les fichiers que je viens de créer : 

```
[leo@node1 ~]$ sudo mount -t auto /dev/secdsk/secdskpart1 /mnt/secdskpart1
[leo@node1 ~]$ sudo mount -t auto /dev/secdsk/secdskpart2 /mnt/secdskpart2
[leo@node1 ~]$ sudo mount -t auto /dev/secdsk/secdskpart3 /mnt/secdskpart3

```

je modifie le fichier : `/etc/fstabs` pour que l'operation soit effective
au démarrage. 

je rajoutes ces lignes : 
```
/dev/secdsk/secdskpart1 /mnt/secdskpart1 ext4 defaults 00
/dev/secdsk/secdskpart2 /mnt/secdskpart2 ext4 defaults 00
/dev/secdsk/secdskpart3 /mnt/secdskpart3 ext4 defaults 00
```

et puis je reboot la VM.

## III. Gestion de services
### 1. Interaction avec un service existant

#### A. Unité simpliste

creation d'un service avec la commande : `sudo nano /etc/systemd/system/web.service`

puis dans ce fichiers j'y mets ces lignes : 
```
[Unit]
Description=Very simple web service

[Service]
ExecStart=/usr/bin/python3 -m http.server 8888

[Install]
WantedBy=multi-user.target

```

ensuite je démarre et j'active le service : 

commande : `sudo systemctl start web` , ` sudo systemctl enable web`

et je vérifie son status avec la commande : `systemctl status web` 

resultat : 
```
[leo@node1 system]$ systemctl status web
● web.service - Very simple web service
   Loaded: loaded (/etc/systemd/system/web.service; enabled; vendor preset: dis>
   Active: active (running) since Sun 2021-09-26 23:10:14 CEST; 30s ago
 Main PID: 1548 (python3)
    Tasks: 1 (limit: 4956)
   Memory: 10.0M
   CGroup: /system.slice/web.service
           └─1548 /bin/python3 -m http.server 8888

Sep 26 23:10:14 node1.tp1.B2 systemd[1]: Started Very simple web service.
```

j'ouvre le port 8888 avec la commande : `sudo firewall-cmd --add-port=8888/tcp` 

et ensuite on peut consulter le server depuis un navigateur a l'adresse :
`http://10.101.1.11:8888`

![](https://i.imgur.com/pW38mkK.png)

#### B. Modification de l'unité

je commence par créer un user `web` avec la commande : `sudo useradd web`

j'ouvre le fichier de mon service qui se trouve à :`/etc/systemd/system/web.service`


et je rajoute ces lignes : 
```
User=web
WorkingDirectory=/srv/webserver
```

ensuite je lance le service avec `sudo systemctl start web` , ` sudo systemctl enable web`

et je vérifie si le server marche en étant log avec web : 
avec : `curl http://10.101.1.11:8888/`
