# TP2 pt. 1 : Gestion de service


## I. Un premier serveur web
### 0. Prérequis

creation de la vm :

modification du nom :
```
[leo@localhost ~]$ echo 'web.tp2.linux' | sudo tee /etc/hostname
web.tp2.linux
```

### 1. Installation

installation d'appache :

installation du paquet `httpd` 

ouverture du port 

trouver le port pour appache : 
```
[leo@web ~]$ sudo ss -alpnt
State      Recv-Q     Send-Q         Local Address:Port         Peer Address:Port    Process
LISTEN     0          128                  0.0.0.0:22                0.0.0.0:*        users:(("sshd",pid=837,fd=5))
LISTEN     0          128                     [::]:22                   [::]:*        users:(("sshd",pid=837,fd=7))
LISTEN     0          128                        *:80                      *:*        users:(("httpd",pid=10264,fd=4),("httpd",pid=10263,fd=4),("httpd",pid=10262,fd=4),("httpd",pid=10260,fd=4))
```

verifiction du status service : 
```
[leo@web ~]$ sudo systemctl status httpd
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor preset: disabled)
   Active: active (running) since Tue 2021-10-05 15:06:55 CEST; 10min ago
     Docs: man:httpd.service(8)
 Main PID: 829 (httpd)
   Status: "Running, listening on: port 80"
    Tasks: 213 (limit: 4956)
   Memory: 29.2M
   CGroup: /system.slice/httpd.service
           ├─829 /usr/sbin/httpd -DFOREGROUND
           ├─849 /usr/sbin/httpd -DFOREGROUND
           ├─850 /usr/sbin/httpd -DFOREGROUND
           ├─851 /usr/sbin/httpd -DFOREGROUND
           └─852 /usr/sbin/httpd -DFOREGROUND

Oct 05 15:06:55 web.tp2.linux systemd[1]: Starting The Apache HTTP Server...
Oct 05 15:06:55 web.tp2.linux httpd[829]: AH00558: httpd: Could not reliably determine the server's fully qualified dom>
Oct 05 15:06:55 web.tp2.linux systemd[1]: Started The Apache HTTP Server.
Oct 05 15:06:56 web.tp2.linux httpd[829]: Server configured, listening on: port 80
```


verification du démarrage automatique
```
[leo@web ~]$ sudo systemctl is-enabled httpd
enabled
```


curl : 
```
[leo@web ~]$ curl localhost
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, 
    
[...]
    
      <a href="https://nginx.org">NGINX&trade;</a> is a registered trademark of <a href="https://">F5 Networks, Inc.</a>.
      </footer>

  </body>
</html>
````

### 2. Avancer vers la maîtrise du service

verification du service : 
```
[leo@web ~]$ sudo systemctl status httpd.service
[sudo] password for leo:
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor preset: disabled)
   Active: active (running) since Tue 2021-10-05 15:06:55 CEST; 33min ago
     Docs: man:httpd.service(8)
 Main PID: 829 (httpd)
   Status: "Total requests: 5; Idle/Busy workers 100/0;Requests/sec: 0.00251; Bytes served/sec:  21 B/sec"
    Tasks: 278 (limit: 4956)
   Memory: 35.1M
   CGroup: /system.slice/httpd.service
           ├─ 829 /usr/sbin/httpd -DFOREGROUND
           ├─ 849 /usr/sbin/httpd -DFOREGROUND
           ├─ 850 /usr/sbin/httpd -DFOREGROUND
           ├─ 851 /usr/sbin/httpd -DFOREGROUND
           ├─ 852 /usr/sbin/httpd -DFOREGROUND
           └─1659 /usr/sbin/httpd -DFOREGROUND

Oct 05 15:06:55 web.tp2.linux systemd[1]: Starting The Apache HTTP Server...
Oct 05 15:06:55 web.tp2.linux httpd[829]: AH00558: httpd: Could not reliably determine the server's fully qualified dom>
Oct 05 15:06:55 web.tp2.linux systemd[1]: Started The Apache HTTP Server.
Oct 05 15:06:56 web.tp2.linux httpd[829]: Server configured, listening on: port 80
```

cat du service : 
```
[leo@web ~]$ sudo cat httpd.service
cat: httpd.service: No such file or directory
[leo@web ~]$ sudo cat /usr/lib/systemd/system/httpd.service
# See httpd.service(8) for more information on using the httpd service.

# Modifying this file in-place is not recommended, because changes
# will be overwritten during package upgrades.  To customize the
# behaviour, run "systemctl edit httpd" to create an override unit.

# For example, to pass additional options (such as -D definitions) to
# the httpd binary at startup, create an override unit (as is done by
# systemctl edit) and enter the following:

#       [Service]
#       Environment=OPTIONS=-DMY_DEFINE

[Unit]
Description=The Apache HTTP Server
Wants=httpd-init.service
After=network.target remote-fs.target nss-lookup.target httpd-init.service
Documentation=man:httpd.service(8)

[Service]
Type=notify
Environment=LANG=C

ExecStart=/usr/sbin/httpd $OPTIONS -DFOREGROUND
ExecReload=/usr/sbin/httpd $OPTIONS -k graceful
# Send SIGWINCH for graceful stop
KillSignal=SIGWINCH
KillMode=mixed
PrivateTmp=true

[Install]
WantedBy=multi-user.target
```

lignes du httpd.conf en rapport avec l'user : 
```
User apache
Group apache
```

le service tourne sur le bon user : 
```
[leo@web ~]$ sudo ps -ef
UID          PID    PPID  C STIME TTY          TIME CMD
apache       849     829  0 15:06 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache       850     829  0 15:06 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache       851     829  0 15:06 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache       852     829  0 15:06 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      1659     829  0 15:24 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
```

l'user a bien acceder au fichiers du site : 
```
[leo@web www]$ ls -al
total 4
drwxr-xr-x.  4 root root   33 Oct  5 14:31 .
drwxr-xr-x. 21 root root 4096 Oct  5 14:31 ..
drwxr-xr-x.  2 root root    6 Jun 11 17:35 cgi-bin
drwxr-xr-x.  2 root root    6 Jun 11 17:35 html
```

creation de l'user `webApache`
```
sudo useradd webApache -s /sbin/nologin -g apache
```

modification du fichier appache : 
```
User webApache
Group apache
```

redemarrage du service : 
```
[leo@web ~]$ sudo systemctl start httpd.service
[leo@web ~]$ sudo systemctl enable httpd.service
[leo@web ~]$ sudo systemctl status httpd.service
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor preset: disabled)
   Active: active (running) since Tue 2021-10-05 15:06:55 CEST; 1h 8min ago
     Docs: man:httpd.service(8)
 Main PID: 829 (httpd)
   Status: "Total requests: 5; Idle/Busy workers 100/0;Requests/sec: 0.00121; Bytes served/sec:  10 B/sec"
    Tasks: 278 (limit: 4956)
   Memory: 35.1M
   CGroup: /system.slice/httpd.service
           ├─ 829 /usr/sbin/httpd -DFOREGROUND
           ├─ 849 /usr/sbin/httpd -DFOREGROUND
           ├─ 850 /usr/sbin/httpd -DFOREGROUND
           ├─ 851 /usr/sbin/httpd -DFOREGROUND
           ├─ 852 /usr/sbin/httpd -DFOREGROUND
           └─1659 /usr/sbin/httpd -DFOREGROUND

Oct 05 15:06:55 web.tp2.linux systemd[1]: Starting The Apache HTTP Server...
Oct 05 15:06:55 web.tp2.linux httpd[829]: AH00558: httpd: Could not reliably determine the server's fully qualified dom>
Oct 05 15:06:55 web.tp2.linux systemd[1]: Started The Apache HTTP Server.
Oct 05 15:06:56 web.tp2.linux httpd[829]: Server configured, listening on: port 80
```

modification du port pour apache :
```
[leo@web ~]$ sudo cat /etc/httpd/conf/httpd.conf

ServerRoot "/etc/httpd"

Listen 1050

Include conf.modules.d/*.conf

User webApache
Group apache
[...]
```

ajout d'une ligne de redirection pour le dossier ou sonts stockés les pages :
```
Include /etc/httpd/sites-enabled
```

ouverture du nouveau port et fermeture de l'ancien dans le firewall :

```
[leo@web ~]$ sudo firewall-cmd --add-port=1050/tcp --permanent
[leo@web ~]$ sudo firewall-cmd --remove-port=80/tcp --permanent
success
```


redemarrage de apache :
```
[leo@web ~]$ sudo systemctl restart httpd.service
[sudo] password for leo:
[leo@web ~]$ sudo systemctl enable httpd.service
[leo@web ~]$ sudo systemctl status httpd.service
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor preset: disabled)
   Active: active (running) since Wed 2021-10-06 09:59:24 CEST; 9s ago
     Docs: man:httpd.service(8)
 Main PID: 1725 (httpd)
   Status: "Running, listening on: port 1050"
    Tasks: 213 (limit: 4956)
   Memory: 24.2M
   CGroup: /system.slice/httpd.service
           ├─1725 /usr/sbin/httpd -DFOREGROUND
           ├─1727 /usr/sbin/httpd -DFOREGROUND
           ├─1728 /usr/sbin/httpd -DFOREGROUND
           ├─1729 /usr/sbin/httpd -DFOREGROUND
           └─1730 /usr/sbin/httpd -DFOREGROUND

Oct 06 09:59:24 web.tp2.linux systemd[1]: httpd.service: Succeeded.
Oct 06 09:59:24 web.tp2.linux systemd[1]: Stopped The Apache HTTP Server.
Oct 06 09:59:24 web.tp2.linux systemd[1]: Starting The Apache HTTP Server...
Oct 06 09:59:24 web.tp2.linux systemd[1]: Started The Apache HTTP Server.
Oct 06 09:59:24 web.tp2.linux httpd[1725]: Server configured, listening on: port 1050
```

## II. Une stack web plus avancée
### 2. Setup


tableau des machines :
| Machine         | IP            | Service                 | Port ouvert | IP autorisées |
|-----------------|---------------|-------------------------|-------------|---------------|
| web.tp2.linux   | 10.102.1.11   | web server              | 80          |               |
| db.tp2.linux    | 10.102.1.12   | database                | 3306        |   10.102.1.11 |

modification de `web.tp2.linux` et création de`db.tp2.linux`, et je fais toutes les étapes de la checklist

creation de la vm `db.tp2.linux` : 

elle porte bien le bon nom : 
```
[leo@localhost ~]$ echo 'db.tp2.linux' | sudo tee /etc/hostname
[sudo] password for leo:
db.tp2.linux
```

elle a bien une ip fixe : 
```
[leo@localhost ~]$
[leo@localhost ~]$ sudo cat /etc/sysconfig/network-scripts/ifcfg-enp0s8
NAME=enp0s8
DEVICE=enp0s8

BOOTPROTO=static
ONBOOT=yes

IPADDR=10.102.1.12
NETMASK=255.255.255.0
```

elle a bien internet et elle peut resoudre des noms : 
```
[leo@localhost ~]$ ping google.com
PING google.com (142.250.178.142) 56(84) bytes of data.
64 bytes from par21s22-in-f14.1e100.net (142.250.178.142): icmp_seq=1 ttl=114 time=21.1 ms
64 bytes from par21s22-in-f14.1e100.net (142.250.178.142): icmp_seq=2 ttl=114 time=20.2 ms
64 bytes from par21s22-in-f14.1e100.net (142.250.178.142): icmp_seq=3 ttl=114 time=20.6 ms
64 bytes from par21s22-in-f14.1e100.net (142.250.178.142): icmp_seq=4 ttl=114 time=20.8 ms
--- google.com ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3005ms
rtt min/avg/max/mdev = 20.199/20.673/21.079/0.321 ms
```

je remet la conf apache par default pour `web.tp2.linux` : 
```
[leo@web ~]$ sudo cat /etc/httpd/conf/httpd.conf

ServerRoot "/etc/httpd"

Listen 80

Include conf.modules.d/*.conf

User apache
Group apache

[...]

IncludeOptional conf.d/*.conf
```

pour `web.tp2.linux` je ré-ouvre le port 80 et je ferme le port utilisé pour la partie précedente : 
```
[leo@web ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
success
[leo@web ~]$ sudo firewall-cmd --remove-port=1050/tcp --permanent
success
```

et je redemarre le service :
```
[leo@web ~]$ sudo systemctl restart httpd.service
[leo@web ~]$ sudo systemctl enable httpd.service
[leo@web ~]$ sudo systemctl status httpd.service
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor preset: disabled)
   Active: active (running) since Wed 2021-10-06 12:02:43 CEST; 14s ago
     Docs: man:httpd.service(8)
 Main PID: 1758 (httpd)
   Status: "Running, listening on: port 80"
    Tasks: 213 (limit: 4956)
   Memory: 32.3M
   CGroup: /system.slice/httpd.service
           ├─1758 /usr/sbin/httpd -DFOREGROUND
           ├─1760 /usr/sbin/httpd -DFOREGROUND
           ├─1761 /usr/sbin/httpd -DFOREGROUND
           ├─1762 /usr/sbin/httpd -DFOREGROUND
           └─1763 /usr/sbin/httpd -DFOREGROUND

Oct 06 12:02:43 web.tp2.linux systemd[1]: httpd.service: Succeeded.
Oct 06 12:02:43 web.tp2.linux systemd[1]: Stopped The Apache HTTP Server.
Oct 06 12:02:43 web.tp2.linux systemd[1]: Starting The Apache HTTP Server...
Oct 06 12:02:43 web.tp2.linux systemd[1]: Started The Apache HTTP Server.
Oct 06 12:02:43 web.tp2.linux httpd[1758]: Server configured, listening on: port 80
```

installation des réfrentiels : 
```
[leo@web ~]$ sudo dnf install epel-release

[...]

Installed:
  epel-release-8-13.el8.noarch

Complete!
```

update avec `dnf update`
```
Installed:
  grub2-tools-efi-1:2.02-99.el8_4.1.1.x86_64

Complete!
```

installation du depot Remi : 
```
sudo dnf install https://rpms.remirepo.net/enterprise/remi-release-8.rpm

[...]

Installed:
  remi-release-8.4-1.el8.remi.noarch

Complete!
```

execution des modules : 
```
[leo@web ~]$ sudo dnf module list php
Safe Remi's RPM repository for Enterprise L 2.9 MB/s | 2.0 MB     00:00
Rocky Linux 8 - AppStream
Name     Stream      Profiles                      Summary
php      7.2 [d]     common [d], devel, minimal    PHP scripting language
php      7.3         common [d], devel, minimal    PHP scripting language
php      7.4         common [d], devel, minimal    PHP scripting language   
Remi's Modular repository for Enterprise Linux 8 - x86_64
Name     Stream      Profiles                      Summary
php      remi-7.2    common [d], devel, minimal    PHP scripting language
php      remi-7.3    common [d], devel, minimal    PHP scripting language
php      remi-7.4    common [d], devel, minimal    PHP scripting language
php      remi-8.0    common [d], devel, minimal    PHP scripting language
php      remi-8.1    common [d], devel, minimal    PHP scripting language   
Hint: [d]efault, [e]nabled, [x]disabled, [i]nstalled
```

recuperation du dernier module php : 
```
[leo@web ~]$ sudo dnf module enable php:remi-7.4
Last metadata expiration check: 0:01:19 ago on Wed 06 Oct 2021 12:33:56 PM CEST.
Dependencies resolved.
============================================================================
 Package          Architecture    Version            Repository        Size
============================================================================
Enabling module streams:
 php                              remi-7.4

Transaction Summary
============================================================================
Is this ok [y/N]: y
Complete!
```

je re-affiche la liste des modules pour vérifier que tout est bien installés : 
```
[leo@web ~]$ sudo dnf module list php
Last metadata expiration check: 0:01:53 ago on Wed 06 Oct 2021 12:33:56 PM CEST.
Rocky Linux 8 - AppStream
Name    Stream         Profiles                     Summary
php     7.2 [d]        common [d], devel, minimal   PHP scripting language
php     7.3            common [d], devel, minimal   PHP scripting language
php     7.4            common [d], devel, minimal   PHP scripting language  
Remi's Modular repository for Enterprise Linux 8 - x86_64
Name    Stream         Profiles                     Summary
php     remi-7.2       common [d], devel, minimal   PHP scripting language
php     remi-7.3       common [d], devel, minimal   PHP scripting language
php     remi-7.4 [e]   common [d], devel, minimal   PHP scripting language
php     remi-8.0       common [d], devel, minimal   PHP scripting language
php     remi-8.1       common [d], devel, minimal   PHP scripting language  
Hint: [d]efault, [e]nabled, [x]disabled, [i]nstalled
```

installation des packages (sauf mariaDB dans `web.tp2.linux`): 
```
[leo@web ~]$ dnf install httpd vim wget zip unzip libxml2 openssl php74-php php74-php-ctype php74-php-curl php74-php-gd php74-php-iconv php74-php-json php74-php-libxml php74-php-mbstring php74-php-openssl php74-php-posix php74-php-session php74-php-xml php74-php-zip php74-php-zlib php74-php-pdo php74-php-mysqlnd php74-php-intl php74-php-bcmath php74-php-gmp

[...]

Complete!
```

installation du package sur `db.tp2.linux` :
```
[leo@db ~]$ sudo dnf install httpd mariadb-server
```

je configure le service en modifiant le fichier suivant :
```
[leo@web ~]$ sudo vim /etc/httpd/sites-available/web.tp2.linux
```
```
<VirtualHost *:80>
  DocumentRoot /var/www/sub-domains/web.tp2.linux/html/
  ServerName  web.tp2.linux

  <Directory /var/www/sub-domains/web.tp2.linux/html/>
    Require all granted
    AllowOverride All
    Options FollowSymLinks MultiViews

    <IfModule mod_dav.c>
      Dav off
    </IfModule>
  </Directory>
</VirtualHost>
```

ensuite je créer un lien vers le fichier : 
`ln -s /etc/httpd/sites-available/web.tp2.linux /etc/httpd/sites-enabled/`

je créer le repertoire : 
`mkdir -p /var/www/sub-domains/web.tp2.linux/html`

je récupère mon timezone avec : 
```
[leo@web ~]$ timedatectl
               Local time: Fri 2021-10-08 14:44:41 CEST
           Universal time: Fri 2021-10-08 12:44:41 UTC
                 RTC time: Fri 2021-10-08 12:44:42
                Time zone: Europe/Berlin (CEST, +0200)
System clock synchronized: no
              NTP service: n/a
          RTC in local TZ: no
```

je modifie le fichier `/etc/opt/remi/php74/php.ini` pour y ajouter ma timezone qui est : `Europe/Berlin`
```
[leo@web ~]$ sudo cat /etc/opt/remi/php74/php.ini | grep 'timezone'
date.timezone = "Europe/Berlin"
```

dans `db.tp2.linux` je configure mariadb :
```
[leo@db ~]$ sudo systemctl enable mariadb
[leo@db ~]$ sudo systemctl restart mariadb

[leo@db ~]$ sudo systemctl status mariadb
● mariadb.service - MariaDB 10.3 database server
   Loaded: loaded (/usr/lib/systemd/system/mariadb.service; enabled; vendor preset: disabled)
   Active: active (running) since Fri 2021-10-08 14:22:38 CEST; 30min ago
    
[...]

Oct 08 14:22:38 db.tp2.linux systemd[1]: Started MariaDB 10.3 database server.
```

### A. Serveur Web et NextCloud
installation de nextcloud :

je recupère le .zip :
```
 wget https://download.nextcloud.com/server/releases/nextcloud-22.2.0.zip
```

j'unzip mon fichier : 
`unzip nextcloud-22.2.0.zip`

et je déplace le contenu dans mon documentRoot :
`cp -Rf * /var/www/sub-domains/web.tp2.linux/html/`

je change le propriétaire du fichier : 
```
chown -Rf apache.apache /var/www/sub-domains/web.tp2.linux/html
```

je deplace le dossier `data` dans le DocumentRoot :
```
mv /var/www/sub-domains/web.tp2.linux/html/data /var/www/sub-domains/web.tp2.linux/
```

je redemarre le service httpd et maria db et puis je peux acceder à l'interface nextCloud depuis un navigateur a l'adresse :
`http://10.102.1.12`

je repère le port utilisé :
```
[leo@db ~]$ sudo ss -alpnt
State     Recv-Q    Send-Q        Local Address:Port         Peer Address:Port    Process
LISTEN    0         80                        *:3306                    *:*        users:(("mysqld",pid=1544,fd=21))
```


### B. Base de données
préparation de la base pour NextCloud : 

je me connecte en root system à mariadb pour changer le mdp root de mariadb : 
```
sudo mysql -u root
```

je change le mot de passe pour l'user root mariadb en local : 
```
ALTER USER 'root'@'localhost' IDENTIFIED BY 'toto';
```

conection a la base avec : `sudo mysql -u root -p`


et je rentre les commandes suivantes : 

creation d'un utilisateur :
```
CREATE USER 'nextcloud'@'10.102.1.11' IDENTIFIED BY 'toto';
```

creation de la base de donnée : 
```
CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
```

je donne tous les droits à l'utilisateur nextcloud sur les tables : 
```
GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.102.1.11';
```

activation des privilèges :
```
FLUSH PRIVILEGES;
```

tentative de connexion a db depuis la VM 1 :
```
[leo@web ~]$ mysql -u nextcloud -h 10.102.1.12 -p
Enter password:
ERROR 2002 (HY000): Can't connect to MySQL server on '10.102.1.12' (115)
```
les logs :
```
2021-10-08 16:55:21 17 [Warning] Access denied for user 'nextcloud'@'db.tp2.linux' (using password: YES)
```

ajout d'un user pour pouvoir acceder a la db depuis `web.tp2.linux` 
```
MariaDB [(none)]> CREATE USER 'nextcloud'@'web.tp2.linux' IDENTIFIED BY 'toto';
Query OK, 0 rows affected (0.000 sec)
```

je lui donne tous les privilèges :
```
MariaDB [(none)]> GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'web.tp2.linux';
Query OK, 0 rows affected (0.000 sec)
```

applications des privilèges :
```
MariaDB [(none)]> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.000 sec)
```
j'ouvre le port `3306` (port mysql) :
```
firewall-cmd --add-port=3306/tcp --permanent
firewall-cmd --add-port=3306/udp --permanent
```
(ca marche mieux quand c'est ouvert^^ j'ai réelement passé 10 min a chercher pourquoi ca ne marchait pas....)
![](https://i.imgur.com/mdmAbgY.gif)


nouvelle tentative de connexion :
```
[leo@web ~]$ mysql -u nextcloud -h 10.102.1.12 -p
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 22
Server version: 10.3.28-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]>
```

affichage de la base de donées : 
```
MariaDB [(none)]> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| nextcloud          |
+--------------------+
2 rows in set (0.001 sec)
```

selection et affichage d'une table spécifique :
```
MariaDB [(none)]> USE information_schema;
Reading table information for completion of table and column names
You can turn off this feature to get a quicker startup with -A

Database changed
MariaDB [information_schema]> SHOW TABLES;
+---------------------------------------+
| Tables_in_information_schema          |
+---------------------------------------+
| ALL_PLUGINS                           |
| APPLICABLE_ROLES                      |
| CHARACTER_SETS                        |
| CHECK_CONSTRAINTS                     |
| COLLATIONS                            |
| COLLATION_CHARACTER_SET_APPLICABILITY |
| COLUMNS                               |
| COLUMN_PRIVILEGES                     |
| ENABLED_ROLES                         |
| ENGINES                               |
| EVENTS                                |
| FILES                                 |
| GLOBAL_STATUS                         |
| GLOBAL_VARIABLES                      |
| KEY_CACHES                            |
| KEY_COLUMN_USAGE                      |
| PARAMETERS                            |
| PARTITIONS                            |
| PLUGINS                               |
| PROCESSLIST                           |
| PROFILING                             |
| REFERENTIAL_CONSTRAINTS               |
| ROUTINES                              |
| SCHEMATA                              |
| SCHEMA_PRIVILEGES                     |
| SESSION_STATUS                        |
| SESSION_VARIABLES                     |
| STATISTICS                            |
| SYSTEM_VARIABLES                      |
| TABLES                                |
| TABLESPACES                           |
| TABLE_CONSTRAINTS                     |
| TABLE_PRIVILEGES                      |
| TRIGGERS                              |
| USER_PRIVILEGES                       |
| VIEWS                                 |
| GEOMETRY_COLUMNS                      |
| SPATIAL_REF_SYS                       |
| CLIENT_STATISTICS                     |
| INDEX_STATISTICS                      |
| INNODB_SYS_DATAFILES                  |
| USER_STATISTICS                       |
| INNODB_SYS_TABLESTATS                 |
| INNODB_LOCKS                          |
| INNODB_MUTEXES                        |
| INNODB_CMPMEM                         |
| INNODB_CMP_PER_INDEX                  |
| INNODB_CMP                            |
| INNODB_FT_DELETED                     |
| INNODB_CMP_RESET                      |
| INNODB_LOCK_WAITS                     |
| TABLE_STATISTICS                      |
| INNODB_TABLESPACES_ENCRYPTION         |
| INNODB_BUFFER_PAGE_LRU                |
| INNODB_SYS_FIELDS                     |
| INNODB_CMPMEM_RESET                   |
| INNODB_SYS_COLUMNS                    |
| INNODB_FT_INDEX_TABLE                 |
| INNODB_CMP_PER_INDEX_RESET            |
| user_variables                        |
| INNODB_FT_INDEX_CACHE                 |
| INNODB_SYS_FOREIGN_COLS               |
| INNODB_FT_BEING_DELETED               |
| INNODB_BUFFER_POOL_STATS              |
| INNODB_TRX                            |
| INNODB_SYS_FOREIGN                    |
| INNODB_SYS_TABLES                     |
| INNODB_FT_DEFAULT_STOPWORD            |
| INNODB_FT_CONFIG                      |
| INNODB_BUFFER_PAGE                    |
| INNODB_SYS_TABLESPACES                |
| INNODB_METRICS                        |
| INNODB_SYS_INDEXES                    |
| INNODB_SYS_VIRTUAL                    |
| INNODB_TABLESPACES_SCRUBBING          |
| INNODB_SYS_SEMAPHORE_WAITS            |
+---------------------------------------+
76 rows in set (0.001 sec)
```

### Finaliser l'installation de NextCloud

modification du fichier hosts sur windows :
```
PS C:\WINDOWS\System32\drivers\etc> nano .\hosts
```
ajout de la ligne dans `.\hosts`
```
10.102.1.11      web.tp2.linux
```

avec un navigateur je me rend sur : `http://web.tp2.linux`


connexion a la base de donnée :
```
[leo@web ~]$ mysql -u nextcloud -h 10.102.1.12 -p
```

recupération du nombre de tables dans la base de données "nextcloud" : 
```
MariaDB [nextcloud]> SELECT count(*) AS TOTALNUMBEROFTABLES FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'nextcloud';
+---------------------+
| TOTALNUMBEROFTABLES |
+---------------------+
|                  96 |
+---------------------+
1 row in set (0.001 sec)
```

