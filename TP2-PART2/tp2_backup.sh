#!/bin/bash
# backup script for linux tp
# Leo.S - 25/10/21

dest=$1
tarName="$(date '+tp2_backup_%Y%m%d_%T' | tr -d :).tar.gz"

tar cvfz $tarName $2 #&>/dev/null

rsync -av --remove-source-files "$tarName" $dest #&>/dev/null
