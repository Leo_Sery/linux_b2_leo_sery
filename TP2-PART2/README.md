# TP2 pt. 2 : Maintien en condition opérationnelle

## I. Monitoring
### 2. Setup
#### Setup Netdata

passage en Root et puis intallation de NetData sur la `web.tp2.linux` et `db.tp2.linux`:
```
bash <(curl -Ss https://my-netdata.io/kickstart.sh)
```

téléchargement des dependances pour rocky linux :
```
wget [...]libuv-devel-1.41.1-1.el8_4.x86_64.rpm
wget [...]autogen-5.18.12-8.el8.1.x86_64.rpm
wget [...]autoconf-archive-2018.03.13-1.el8.noarch.rpm
```

installation des dependances : 
```
dnf install libuv-devel-1.41.1-1.el8_4.x86_64.rpm
dnf install autogen-5.18.12-8.el8.1.x86_64.rpm
dnf install autoconf-archive-2018.03.13-1.el8.noarch.rpm
```

verification du status de netdata et activation du service au boot de la machine :
```
[leo@web ~]$ sudo systemctl restart netdata
[leo@web ~]$ sudo systemctl status netdata
● netdata.service - Real time performance monitoring
   Loaded: loaded (/usr/lib/systemd/system/netdata.service; enabled; vendor preset: disabled)
   Active: active (running) since Mon 2021-10-11 12:21:10 CEST; 29s ago
```


#### Manipulation du service Netdata

et aussi pour db.tp2.linux :
```
[leo@db ~]$ sudo systemctl enable netdata
[leo@db ~]$ sudo systemctl status netdata
● netdata.service - Real time performance monitoring
   Loaded: loaded (/usr/lib/systemd/system/netdata.service; enabled; vendor preset: disabled)
   Active: active (running) since Tue 2021-10-12 09:39:43 CEST; 15min ago
 [...]
```

detection du port utilisé par netdata :
```
[leo@db ~]$ sudo ss -altpn
[sudo] password for leo:
State     Recv-Q    Send-Q       Local Address:Port        Peer Address:Port    Process
LISTEN    0         128                0.0.0.0:19999            0.0.0.0:*        users:(("netdata",pid=36515,fd=6))
```

le service tourne sur le port `19999`

ouverture du port 19999 sur les deux VM :
```
sudo firewall-cmd --add-port=19999/tcp
sudo firewall-cmd --add-port=19999/tcp --permanent
```

#### Setup Alerting

modification de la conf NetData pour ajouter une alarme discord : 
```
[leo@web netdata]$ sudo cat /etc/netdata/health_alarm_notify.conf
###############################################################################
# sending discord notifications

# note: multiple recipients can be given like this:
#                  "CHANNEL1 CHANNEL2 ..."

# enable/disable sending discord notifications
SEND_DISCORD="YES"

# Create a webhook by following the official documentation -
# https://support.discordapp.com/hc/en-us/articles/228383668-Intro-to-Webhooks
DISCORD_WEBHOOK_URL="https://discord.com/api/webhooks/897394745024872469/nvh3-ihUtXtHxKYeeSU2KS_AMaycu2_QB7TYaoUOGvjzmeSs1RCdKNQT4Ru6RMV6oLM3"

# if a role's recipients are not configured, a notification will be send to
# this discord channel (empty = do not send a notification for unconfigured
# roles):
DEFAULT_RECIPIENT_DISCORD="alarms"
```

on vérifie si les alertes marchent avec la commande : 
```
[leo@web netdata]$ sudo bash -x /usr/libexec/netdata/plugins.d/alarm-notify.sh test "sysadmin"
```

le résultat :

![](https://i.imgur.com/crsE0rj.png)

#### Config alerting

creation d'une alarme pour la ram :

on définit l'alarme pour 50% 

```
[leo@web health.d]$ sudo cat ram-usage.conf
[sudo] password for leo:
alarm: ram_usage
    on: system.ram
lookup: average -1m percentage of used
 units: %
 every: 1m
  warn: $this > 50
  crit: $this > 90
  info: The percentage of RAM being used by the system.
```

idem dans la deuxieme VM `db.tp2.linux` :
```
[leo@db ~]$ sudo cat /etc/netdata/health.d/ram-usage.conf
alarm: ram_usage
    on: system.ram
lookup: average -1m percentage of used
 units: %
 every: 1m
  warn: $this > 50
  crit: $this > 90
  info: The percentage of RAM being used by the system.
```

puis pour les deux VM on redemarre le service :
```
[leo@web ~]$ sudo systemctl restart netdata
[sudo] password for leo:
[leo@web ~]$ sudo systemctl status netdata
● netdata.service - Real time performance monitoring
   Loaded: loaded (/usr/lib/systemd/system/netdata.service; enabled; vendor preset: disabled)
   Active: active (running) since Tue 2021-10-12 10:42:36 CEST; 4s ago
```

et 

```
[leo@db ~]$ sudo systemctl restart netdata
[leo@db ~]$ sudo systemctl status netdata
● netdata.service - Real time performance monitoring
   Loaded: loaded (/usr/lib/systemd/system/netdata.service; enabled; vendor preset: disabled)
   Active: active (running) since Tue 2021-10-12 10:50:07 CEST; 34s ago
```

maintenant on test notre alarme avec un stress de la ram :

installation de stress avec la commande : `sudo dnf install stress` sur les deux VM.

on lance une periode de stress avec la commande :
```
[leo@web ~]$ stress --vm 2 --timeout 180
```

résultat :

![](https://i.imgur.com/P8mhyRH.png)


## II. Backup
### Partage NFS

creation de la vm de back , puis checklist :

elle à le bon nom :
```
[leo@backup ~]$ hostname
backup.tp2.linux
```

elle a acces à internet et elle peut resoudre des noms :
```
[leo@backup ~]$ ping 1.1.1.1
PING 1.1.1.1 (1.1.1.1) 56(84) bytes of data.
64 bytes from 1.1.1.1: icmp_seq=1 ttl=57 time=16.2 ms
64 bytes from 1.1.1.1: icmp_seq=2 ttl=57 time=17.0 ms
^C
--- 1.1.1.1 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 16.218/16.628/17.038/0.410 ms
[leo@backup ~]$ ping google.com
PING google.com (142.250.179.78) 56(84) bytes of data.
64 bytes from par21s19-in-f14.1e100.net (142.250.179.78): icmp_seq=1 ttl=118 time=14.9 ms
64 bytes from par21s19-in-f14.1e100.net (142.250.179.78): icmp_seq=2 ttl=118 time=15.0 ms
64 bytes from par21s19-in-f14.1e100.net (142.250.179.78): icmp_seq=3 ttl=118 time=14.2 ms
64 bytes from par21s19-in-f14.1e100.net (142.250.179.78): icmp_seq=4 ttl=118 time=14.9 ms
^C
--- google.com ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3006ms
rtt min/avg/max/mdev = 14.232/14.774/15.006/0.326 ms
```

creation des dossiers pour la backup :
```
[leo@backup ~]$ sudo mkdir /srv/backups
[sudo] password for leo:
[leo@backup ~]$ cd /srv/backups/
[leo@backup backups]$ sudo mkdir web.tp2.linux
[leo@backup backups]$ sudo mkdir db.tp2.linux
```

installation de nfs sur la machine de backup :
```
[leo@backup backups]$ sudo dnf -y install nfs-utils
Installed:
  gssproxy-0.8.0-19.el8.x86_64             keyutils-1.5.10-6.el8.x86_64            libevent-2.1.8-5.el8.x86_64
  libverto-libevent-0.3.0-5.el8.x86_64     nfs-utils-1:2.3.3-41.el8_4.2.x86_64     python3-pyyaml-3.12-12.el8.x86_64
  quota-1:4.04-12.el8.x86_64               quota-nls-1:4.04-12.el8.noarch          rpcbind-1.2.5-8.el8.x86_64

Complete!
```

on modifie le fichier `/etc/idmapd.conf` pour y ajouter la ligne :
```
Domain = backup.tp2.linux
```

on modifie le fichier `/etc/exports` pour y ajouter les lignes :
```
/srv/backups/web.tp2.linux 10.102.1.11/24(rw,no_root_squash)
/srv/backups/db.tp2.linux 10.102.1.12/24(rw,no_root_squash)
```

puis on redemarre le service :
```
[leo@backup ~]$ sudo systemctl start rpcbind nfs-server
[leo@backup ~]$ sudo systemctl enable rpcbind nfs-server
Created symlink /etc/systemd/system/multi-user.target.wants/nfs-server.service → /usr/lib/systemd/system/nfs-server.service.

[leo@backup ~]$ sudo systemctl status rpcbind nfs-server
● rpcbind.service - RPC Bind
   Loaded: loaded (/usr/lib/systemd/system/rpcbind.service; enabled; vendor preset: enabled)
   Active: active (running) since Sun 2021-10-24 19:59:43 CEST; 1min 41s ago
     Docs: man:rpcbind(8)
 Main PID: 10494 (rpcbind)
    Tasks: 1 (limit: 4956)
   Memory: 1.5M
   CGroup: /system.slice/rpcbind.service
           └─10494 /usr/bin/rpcbind -w -f

Oct 24 19:59:43 backup.tp2.linux systemd[1]: Starting RPC Bind...
Oct 24 19:59:43 backup.tp2.linux systemd[1]: Started RPC Bind.

● nfs-server.service - NFS server and services
   Loaded: loaded (/usr/lib/systemd/system/nfs-server.service; enabled; vendor preset: disabled)
  Drop-In: /run/systemd/generator/nfs-server.service.d
           └─order-with-mounts.conf
   Active: active (exited) since Sun 2021-10-24 19:59:43 CEST; 1min 40s ago
 Main PID: 10517 (code=exited, status=0/SUCCESS)
    Tasks: 0 (limit: 4956)
   Memory: 0B
   CGroup: /system.slice/nfs-server.service

Oct 24 19:59:43 backup.tp2.linux systemd[1]: Starting NFS server and services...
Oct 24 19:59:43 backup.tp2.linux systemd[1]: Started NFS server and services.
```

ensuite on configure le firewall pour ce nouveau service :
```
[leo@backup ~]$ sudo firewall-cmd --add-service=nfs
success
[leo@backup ~]$ sudo firewall-cmd --add-service=nfs --permanent
success
```

on monte les dossiers sur les vm :
```
[leo@web ~]$ sudo mount -t nfs 10.102.1.13:/srv/backups/web.tp2.linux /mnt/backup/

[leo@web ~]$ sudo mount -a
[leo@web ~]$ df -h
Filesystem                              Size  Used Avail Use% Mounted on
devtmpfs                                388M     0  388M   0% /dev
tmpfs                                   405M  304K  405M   1% /dev/shm
tmpfs                                   405M  5.6M  400M   2% /run
tmpfs                                   405M     0  405M   0% /sys/fs/cgroup
/dev/mapper/rl-root                     6.2G  3.8G  2.4G  62% /
/dev/sda1                              1014M  234M  781M  24% /boot
tmpfs                                    81M     0   81M   0% /run/user/1000
10.102.1.13:/srv/backups/web.tp2.linux  6.2G  1.9G  4.4G  30% /mnt/backup
```

modification de `fstab` pour monter la partition au démarrage :
```
10.102.1.13:/srv/backups/web.tp2.linux /mnt/backup nfs defaults 0 0
```

on fait la meme chose pour db.tp2.linux :
```
10.102.1.13:/srv/backups/db.tp2.linux /mnt/backup/ nfs defaults 0 0
```

modification `fstab` :
```
10.102.1.13:/srv/backups/db.tp2.linux /mnt/backup/ nfs defaults 0 0
```

### Backup de fichiers 

creation du script de backup 
```
[leo@web srv]$ sudo ./tp2_backup.sh /mnt/backup /home/leo/testfolder
tar: Removing leading `/' from member names
/home/leo/testfolder/
sending incremental file list
tp2_backup_20211025_205507.tar.gz

sent 245 bytes  received 43 bytes  576.00 bytes/sec
total size is 127  speedup is 0.44
```

listes des backup sur la vm `backup.tp2.linux` :
```
[leo@backup web.tp2.linux]$ ls
tp2_backup_20211025_204906.tar.gz  tp2_backup_20211025_205424.tar.gz  tp2_backup_20211025_205507.tar.gz
```

### 4. Unité de service

creation d'un fichier de service : 
```
[leo@web system]$ sudo touch tp2_backup.service
```

ajout de la conf dans le fichier `tp2_backup.service` :
```
[Unit]
Description=Our own lil backup service (TP2)

[Service]
ExecStart=/srv/tp2_backup.sh /mnt/backup /home/leo/testfolder
Type=oneshot
RemainAfterExit=no

[Install]
WantedBy=multi-user.target
```

on demarre le serive :
```
[leo@web system]$ sudo systemctl daemon-reload
[leo@web system]$ sudo systemctl start tp2_backup
```

on vérifie la présence de la nouvelle archive : 
```
[leo@web system]$ ls /mnt/backup
tp2_backup_20211025_204906.tar.gz  tp2_backup_20211025_205507.tar.gz
tp2_backup_20211025_205424.tar.gz  tp2_backup_20211025_210834.tar.gz
```

#### B. Timer

création du fichier `tp2_backup.timer` 

j'ajoute la conf dans le fichier :
```
[leo@web system]$ cat tp2_backup.timer
[Unit]
Description=Periodically run our TP2 backup script
Requires=tp2_backup.service

[Timer]
Unit=tp2_backup.service
OnCalendar=*-*-* *:*:00

[Install]
WantedBy=timers.target
```

on relance le service, on l'active au démarrage et on affiche son status pour vérifier :
```
[leo@web system]$ sudo systemctl daemon-reload
[leo@web system]$ sudo systemctl start tp2_backup.timer
[leo@web system]$ sudo systemctl status tp2_backup.timer
● tp2_backup.timer - Periodically run our TP2 backup script
   Loaded: loaded (/etc/systemd/system/tp2_backup.timer; enabled; vendor preset: disabled)
   Active: active (waiting) since Mon 2021-10-25 21:26:11 CEST; 4min 2s ago
  Trigger: Mon 2021-10-25 21:31:00 CEST; 46s left

Oct 25 21:26:11 web.tp2.linux systemd[1]: Started Periodically run our TP2 backup script.
```

#### C. Contexte

modification du serivce pour sauvegarder les fichiers d'appache :
```
[Unit]
Description=Our own lil backup service (TP2)

[Service]
ExecStart=/srv/tp2_backup.sh /mnt/backup /etc/httpd
Type=oneshot
RemainAfterExit=no

[Install]
WantedBy=multi-user.target
```

modification du timer :
```
[leo@web system]$ cat tp2_backup.timer
[Unit]
Description=Periodically run our TP2 backup script
Requires=tp2_backup.service

[Timer]
Unit=tp2_backup.service
OnCalendar=*-*-* 03:15:00

[Install]
WantedBy=timers.target
```

on regarde la liste des timers : 
```
[leo@web system]$ sudo systemctl list-timers
NEXT                          LEFT          LAST                          PASSED       UNIT                         ACT>
Tue 2021-10-26 03:15:00 CEST  5h 24min left Mon 2021-10-25 21:49:04 CEST  1min 36s ago tp2_backup.timer             tp2>
Tue 2021-10-26 19:25:34 CEST  21h left      Mon 2021-10-25 19:25:34 CEST  2h 25min ago systemd-tmpfiles-clean.timer sys>
n/a                           n/a           n/a                           n/a          dnf-makecache.timer          dnf>
3 timers listed.
```

## III. Reverse Proxy
### 2. Setup simple


installation de `nginx` et `epel`  :
```
[leo@front ~]$ sudo dnf install nginx
[...]
Complete!
```

activation du service :
```
[leo@front ~]$ sudo systemctl enable --now nginx
Created symlink /etc/systemd/system/multi-user.target.wants/nginx.service → /usr/lib/systemd/system/nginx.service.

[leo@front ~]$ curl localhost
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
[...]
      <a href="http://www.rockylinux.org/"><img
            src="poweredby.png"
            alt="[ Powered by Rocky Linux ]"
            width="88" height="31" /></a>

      </div>
    </div>
  </body>
</html>
```
dans le fichier `/etc/nginx/nginx.conf` on peut voir que l'utilisateur par default est : `nginx`

je modifie le fichier `/etc/nginx/nginx.conf` pour retirer la partie "server"

et puis je modifie la conf du fichier `/etc/nginx/conf.d/web.tp2.linux.conf` pour y ajouter :
```
[leo@front ~]$ sudo cat /etc/nginx/conf.d/web.tp2.linux.conf
server {
    # on demande à NGINX d'écouter sur le port 80 pour notre NextCloud
    listen 80;

    # ici, c'est le nom de domaine utilisé pour joindre l'application
    # ce n'est pas le nom du reverse proxy, mais le nom que les clients devront saisir pour atteindre le site
    server_name web.tp2.linux; # ici, c'est le nom de domaine utilisé pour joindre l'application (pas forcéme

    # on définit un comportement quand la personne visite la racine du site (http://web.tp2.linux/)
    location / {
        # on renvoie tout le trafic vers la machine web.tp2.linux
        proxy_pass http://web.tp2.linux;
    }
}
```

et pour vérifier on fait un curl : 
```
[leo@front ~]$ curl localhost
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
  <head>
[...]

  <a href="http://www.rockylinux.org/"><img
            src="poweredby.png"
            alt="[ Powered by Rocky Linux ]"
            width="88" height="31" /></a>

      </div>
    </div>
  </body>
</html>
```
on peut voir que cela affiche la page du serveur web.

tableau de machines : 
| Machine             | IP               | Service                 | Port ouvert     | IPs autorisées                               |
|---------------------|------------------|-------------------------|-----------------|----------------------------------------------|
| `web.tp2.linux`     | `10.102.1.11/24` | Serveur Web             | 22, 80, 19999   | 10.102.1.1/24, 10.102.1.14/24                |
| `db.tp2.linux`      | `10.102.1.12/24` | Serveur Base de Données | 22, 3306, 19999 | 10.102.1.1/24, 10.102.1.11/24                |
| `backup.tp2.linux`  | `10.102.1.13/24` | Serveur de backup       | 22, 2049, 19999 | 10.102.1.1/24, 10.102.1.11/24, 10.102.1.12/24|
| `front.tp2.linux`   | `10.102.1.14/24` | Serveur de front        | 22, 80, 19999   | toutes les ip                                |

## IV. Firewalling

![](https://i.imgur.com/NBunOmY.png)
